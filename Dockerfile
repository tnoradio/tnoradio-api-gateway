FROM node:12
RUN mkdir -p /usr/src
WORKDIR /usr/src
COPY package.json /usr/src
RUN npm install 
COPY . /usr/src
EXPOSE 9876
EXPOSE 8081
EXPOSE 3500
CMD npm start