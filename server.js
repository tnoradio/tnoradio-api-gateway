const path = require('path');
const gateway = require('express-gateway');
const http = require('http');
const helmet = require('helmet');
const cors = require('cors');
var expressJwt=require('express-jwt');
var expressJwt = require('express-jwt');
var bodyParser=require('body-parser');
var jwt = require('jsonwebtoken');

const express = require('express')
const app = express()
const port = 3400

gateway()
  .load(path.join(__dirname, 'config'))
  .run();



// adding Helmet to enhance your API's security
app.use(helmet());

// enabling CORS for all requests
app.use(cors());

var jwtClave="laclave_de_cecilio";
app.use(express.static('publica'));
app.use(bodyParser.json());
app.use(expressJwt({secret:jwtClave, algorithms: ['HS256']}).unless({path: ["/login"]}));

var usuario= {
  nombre:"cecilio",
  clave:"cecilio"
 }
 var noticias = [{
  id: 1,
  titulo: "noticia 1"
 }];
  
 app.get('/noticias', function(req, res) {
  res.send(noticias);
 });
  
  
 app.post("/login",function(request,response) {
  
 if (request.body.nombre==usuario.nombre || request.body.clave==usuario.clave) {
  
  var token = jwt.sign({ usuario:"cecilio" }, jwtClave);


  
  response.send(token);
  
 }else {
  
  response.status(401).end("usuario incorrecto")
 }
  
 });

app.get('/hi', (req, res) => {
  res.send("HOLA");
})


  app.get('/api/shows', (req, res) => {

    http.get('localhost:8081/api/shows/health',
    (resp) => {
    let data = '';

      // A chunk of data has been received.
      resp.on('data', (chunk) => {
        data += chunk;
      });

      // The whole response has been received. Print out the result.
      resp.on('end', () => {
        console.log(data);      
        res.send({
            msg: data
          })
      })
    }) 
  })
  
  app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
  })